
import pytest

from os import getcwd, getpid
from os.path import join as path_join

from widediaper.r_session.session_helpers import get_outfolder, get_df_path

def describe_get_df_destination():

    def no_outfolder_specified_uses_tmpdir_plus_widediaper():
        assert get_outfolder("", "20") == "/tmp/widediaper/{}".format("20")

    def absolute_outfolder_specified_uses_absolute_path():
        assert get_outfolder("/tmp/here/path", "20") == "/tmp/here/path"

    def relative_outfolder_uses_cwd():
        dirname = "testing"
        actual_result = get_outfolder(dirname, "20", create_folder=False)
        assert actual_result == path_join(getcwd(), dirname)


def test_get_df_path():

    assert get_df_path('/tmp/st', "to_r", "my_df") == '/tmp/st/to_r_my_df.csv'
