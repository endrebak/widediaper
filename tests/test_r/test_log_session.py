import pytest

from widediaper.r_session.log_session import create_print_and_execute_string, opposite_quotesymbol


def describe_create_print_and_execute_string():

    def handles_simple_input():

        command = '1 + 1'
        actual_result = create_print_and_execute_string(command)

        expected_result = ('print("1 + 1")\n'
                           '1 + 1\n\n')

        assert actual_result == expected_result

    def handles_quirky_input():

        command = '''print('hello world')'''

        actual_result = create_print_and_execute_string(command)

        expected_result = ('''print("print('hello world')")\n'''
                            "print('hello world')\n\n")


        assert actual_result == expected_result


def describe_opposite_quotesymbol():

    def handles_no_quotes_correctly():

        assert opposite_quotesymbol('hello there!') == '"'

    def gets_correct_quote_symbol():

        assert opposite_quotesymbol('''print('hello!')''') == '"'

    def raises_assertionerror_on_mismatched_symbols():

        with pytest.raises(AssertionError):

            opposite_quotesymbol('''print('hello there!")''')
