import pytest

from sys import stdout, stderr

import pandas as pd

from widediaper.utils.helper_functions import StringIO

from widediaper.r_session.session import R


@pytest.mark.integration
def describe_R():

    def describe_call():

        def does_not_choke_when_command_includes_repl_symbol():

            r = R()

            r("'aaaa > hello'")
            result = r("1+1")

            assert result == 2


        def simple_command():

            r = R()

            assert 2 == r("1 + 1")


    def describe_load_library():

        def raises_importerror_on_nonexisting_library():
            r = R()

            with pytest.raises(ImportError):
                r.load_library("hello")


        def loads_library_if_exists():

            r = R()
            r.load_library("stats")


    def describe_writes_commands_to_stream():

        def when_outstream_is_prints_commands_to_stderr(capsys):
            r = R(outstream=True)
            command = "print('hello_world')"
            r(command)

            _, err = capsys.readouterr()
            assert command in err


        def silent_by_default(capsys):
            r = R()
            command = "print('hello_world')"
            r(command)

            _, err = capsys.readouterr()

            assert not err

    def describe_writes_commands_to_file():

        def writes_logfile_when_outfile_not_empty_str(outfolder):

            r = R(outstream=False, session_folder=outfolder)

            command = "1+1"
            r(command)

            print("in the test the outfolder is", outfolder)

            with open(outfolder + "/widediaper.R") as script_handle:
                contents = script_handle.readlines()

            assert "\n".join(contents).count(command) == 2


    def describe_send():


        def dfs_sent_to_r_properly(test_df, expected_df):

            r = R(outstream=False)

            df_name = "did_this_df_arrive"
            r.send(test_df, df_name)
            returned_df = r.get(df_name)

            assert returned_df.equals(expected_df)

        def series_sent_to_r_properly(test_series, expected_series_df):


            r = R(outstream=False)
            series_name = "testing"
            r.send(test_series, series_name)

            series_out = r.get(series_name)

            print("test_series")
            print(test_series)
            print("series_out")
            print(series_out)
            print("expected_series_df")
            print(expected_series_df)
            assert series_out.equals(expected_series_df)


    def describe_get():

        def header_gotten_properly():

            r = R(outstream=False)

            mtcars = r.get("mtcars")

            assert list(mtcars.columns) == ["mpg", "cyl", "disp", "hp", "drat",
                                            "wt", "qsec", "vs", "am", "gear",
                                            "carb"]

        def rownames_gotten_properly():

            r = R(outstream=False)

            mtcars = r.get("mtcars")

            print(mtcars)

            expected_rownames = ["Mazda RX4", "Mazda RX4 Wag", "Datsun 710",
                                 "Hornet 4 Drive", "Hornet Sportabout",
                                 "Valiant", "Duster 360", "Merc 240D",
                                 "Merc 230", "Merc 280", "Merc 280C",
                                 "Merc 450SE", "Merc 450SL", "Merc 450SLC",
                                 "Cadillac Fleetwood", "Lincoln Continental",
                                 "Chrysler Imperial", "Fiat 128", "Honda Civic",
                                 "Toyota Corolla", "Toyota Corona",
                                 "Dodge Challenger", "AMC Javelin", "Camaro Z28",
                                 "Pontiac Firebird", "Fiat X1-9",
                                 "Porsche 914-2", "Lotus Europa",
                                 "Ford Pantera L", "Ferrari Dino",
                                 "Maserati Bora", "Volvo 142E"]

            assert list(mtcars.index.values) == expected_rownames


        def data_gotten_properly(mtcars):

            r = R(outstream=False)

            mtcars_from_r = r.get("mtcars")

            assert mtcars_from_r.equals(mtcars)


@pytest.fixture
def mtcars():

    return pd.read_table(StringIO(u"""
                      mpg  cyl   disp   hp  drat     wt   qsec  vs  am  gear     carb
Mazda RX4            21.0    6  160.0  110  3.90  2.620  16.46   0   1     4        4
Mazda RX4 Wag        21.0    6  160.0  110  3.90  2.875  17.02   0   1     4        4
Datsun 710           22.8    4  108.0   93  3.85  2.320  18.61   1   1     4        1
Hornet 4 Drive       21.4    6  258.0  110  3.08  3.215  19.44   1   0     3        1
Hornet Sportabout    18.7    8  360.0  175  3.15  3.440  17.02   0   0     3        2
Valiant              18.1    6  225.0  105  2.76  3.460  20.22   1   0     3        1
Duster 360           14.3    8  360.0  245  3.21  3.570  15.84   0   0     3        4
Merc 240D            24.4    4  146.7   62  3.69  3.190  20.00   1   0     4        2
Merc 230             22.8    4  140.8   95  3.92  3.150  22.90   1   0     4        2
Merc 280             19.2    6  167.6  123  3.92  3.440  18.30   1   0     4        4
Merc 280C            17.8    6  167.6  123  3.92  3.440  18.90   1   0     4        4
Merc 450SE           16.4    8  275.8  180  3.07  4.070  17.40   0   0     3        3
Merc 450SL           17.3    8  275.8  180  3.07  3.730  17.60   0   0     3        3
Merc 450SLC          15.2    8  275.8  180  3.07  3.780  18.00   0   0     3        3
Cadillac Fleetwood   10.4    8  472.0  205  2.93  5.250  17.98   0   0     3        4
Lincoln Continental  10.4    8  460.0  215  3.00  5.424  17.82   0   0     3        4
Chrysler Imperial    14.7    8  440.0  230  3.23  5.345  17.42   0   0     3        4
Fiat 128             32.4    4   78.7   66  4.08  2.200  19.47   1   1     4        1
Honda Civic          30.4    4   75.7   52  4.93  1.615  18.52   1   1     4        2
Toyota Corolla       33.9    4   71.1   65  4.22  1.835  19.90   1   1     4        1
Toyota Corona        21.5    4  120.1   97  3.70  2.465  20.01   1   0     3        1
Dodge Challenger     15.5    8  318.0  150  2.76  3.520  16.87   0   0     3        2
AMC Javelin          15.2    8  304.0  150  3.15  3.435  17.30   0   0     3        2
Camaro Z28           13.3    8  350.0  245  3.73  3.840  15.41   0   0     3        4
Pontiac Firebird     19.2    8  400.0  175  3.08  3.845  17.05   0   0     3        2
Fiat X1-9            27.3    4   79.0   66  4.08  1.935  18.90   1   1     4        1
Porsche 914-2        26.0    4  120.3   91  4.43  2.140  16.70   0   1     5        2
Lotus Europa         30.4    4   95.1  113  3.77  1.513  16.90   1   1     5        2
Ford Pantera L       15.8    8  351.0  264  4.22  3.170  14.50   0   1     5        4
Ferrari Dino         19.7    6  145.0  175  3.62  2.770  15.50   0   1     5        6
Maserati Bora        15.0    8  301.0  335  3.54  3.570  14.60   0   1     5        8
Volvo 142E           21.4    4  121.0  109  4.11  2.780  18.60   1   1     4        2
    """), header=0, index_col=0, sep="\s\s+")

@pytest.fixture
def test_df():

    df_string = u"\n".join(['1 2 3',
                            '4 5 6',
                            '7 8 9'])

    return pd.read_table(StringIO(df_string), sep="\s+")

@pytest.fixture
def test_series():

    return pd.Series(["a", "b", "c", "d"])

@pytest.fixture
def expected_series_df():

    series_string = u"a\n 1 b\n 2  c\n  3  d"
    return pd.read_table(StringIO(series_string), header=0, sep="\s+")


@pytest.fixture
def expected_df(test_df):

    expected_df = test_df.copy()
    expected_df.columns = ["X1", "X2", "X3"]

    return expected_df


@pytest.fixture
def outfolder(tmpdir):

    return str(tmpdir.mkdir("test"))
