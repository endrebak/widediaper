
import pytest

from widediaper.parse.check_command import check_command, IllegalRCommandException

def describe_parse_command():

    def test_parse_command_with_wrong_order_parens():
        with pytest.raises(IllegalRCommandException):
            check_command("first_lines <- head)df(")


    def test_parse_command_with_wrong_number_parens():
        with pytest.raises(IllegalRCommandException):
            check_command("first_lines <- head(df))")


    def test_parse_command_with_wrong_number_single_quotes():
        with pytest.raises(IllegalRCommandException):
            check_command("print('hello bioinformaticians!)")


    def test_parse_command_with_wrong_number_quotes():
        with pytest.raises(IllegalRCommandException):
            check_command('print("""hello bioinformaticians!)')

    def test_parse_command_with_correct_number_parens():
        check_command('head(df)')

    def test_parse_command_with_correct_number_quotes():
        check_command('head("df")')
