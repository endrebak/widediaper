
import pytest

from widediaper.parse.output import return_correct_type


def describe_return_correct_type():

    def returns_int():
        actual_result = return_correct_type("1")
        assert type(actual_result) == int

    def returns_float():
        actual_result = return_correct_type("1.5")
        assert type(actual_result) == float
