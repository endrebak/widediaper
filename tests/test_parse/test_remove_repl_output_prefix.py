import pytest


from widediaper.parse.output import remove_repl_output_prefix

def describe__remove_repl_output_prefix():

    def works_with_empty_string():
        assert remove_repl_output_prefix("") == ""

    def one_line_only():
        assert remove_repl_output_prefix("[1] hello world!") == "hello world!"

    def multiple_lines(installed_packages_output, installed_packages_output_pruned):

        print("actual", remove_repl_output_prefix(installed_packages_output))
        print("expected", installed_packages_output_pruned)
        assert remove_repl_output_prefix(installed_packages_output) == installed_packages_output_pruned



@pytest.fixture
def installed_packages_output():

    return '''[1] "aroma.affymetrix"   "aroma.core"         "BiocParallel"
  [4] "Category"           "crayon"             "edgeR"
  [7] "GenomicRanges"      "GOstats"            "graph"
 [10] "GSEABase"           "gtools"             "IRanges"
 [13] "limma"              "memoise"            "RBGL"
 [16] "reactome.db"        "R.filesets"         "rtracklayer"
 [19] "R.utils"            "testthat"           "XML"'''


@pytest.fixture
def installed_packages_output_pruned():

    return '''"aroma.affymetrix"   "aroma.core"         "BiocParallel"
"Category"           "crayon"             "edgeR"
"GenomicRanges"      "GOstats"            "graph"
"GSEABase"           "gtools"             "IRanges"
"limma"              "memoise"            "RBGL"
"reactome.db"        "R.filesets"         "rtracklayer"
"R.utils"            "testthat"           "XML"'''
