wide diapeR (pre-alpha)
==============

widediaper is a simple wrapper around an R session so that R can be used from Python.

The ultimate goal of widediaper is to enable calling R libraries in Python code without creating maintenance problems or inscrutable bugs for developers or end-users.

It aims to

1. always work
2. make debugging any called R code simple

It has a restricted set of features by design and is for `*nix` only (tested on Python 2.7 and 3.3 on OS X and Ubuntu).

## Changelog

> v0.0.8 (2015.29.09): fix bug when sending Series (first datapoint was interpreted as header)

> v0.0.7 (2015.28.09): print R errors to stderr, add 'exit_on_error' flag.

## Install

To install widediaper the easiest way is

`pip install widediaper`

This will also install its dependencies, namely `pandas` and `pexpect`.

The only reason to clone this repo is for development (the pip install does not include unit or integration tests.)

## Examples

The examples below serve to document widediaper usage.

### Example 1: Automatic handling of floats and integers

```Python
from widediaper import R
r = R()

# add 1 and 2 in R, return the result to Python
result = r("1 + 2")
print(result)
# 3
print(type(result))
# int
```

### Example 2: No automatic handling of vectors or dataframes
```Python
from widediaper import R
r = R()

result2 = r("1:50")
print(result2)
# '1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25\r\n26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 50'
print(type(result2))
# str
```

### Example 3: Use "get" to get vectors or dataframes from R
```Python
from widediaper import R
r = R()

r("numbers = 1:50")
nums = r.get("numbers")
print(nums)
#      x
# 1    1
# 2    2
# 3    3
print(type(nums))
# pandas.core.frame.DataFrame

# getting the built-in dataframe "mtcars" from R as a pandas DataFrame
mtcars = r.get("mtcars")
print(mtcars)
# mpg  cyl   disp   hp  drat     wt   qsec  vs  am  gear
# Mazda RX4            21.0    6  160.0  110  3.90  2.620  16.46   0   1     4
# Mazda RX4 Wag        21.0    6  160.0  110  3.90  2.875  17.02   0   1     4
# Datsun 710           22.8    4  108.0   93  3.85  2.320  18.61   1   1     4
#...

print(type(mtcars))
# pandas.core.frame.DataFrame
```

### Example 4: Get library data from R

```Python
from widediaper import R

r = R(outstream=False)
# load the Gene Ontology library (not available in Python)
# notice the special method available for library loading (raises ImportError in Python if library not found in R)
r.load_library("GO.db")

# get id to term mapping as dataframe
r("go_df <- as.data.frame(GOTERM)")

# pass the df to Python
ontology_df = r.get("go_df")
ontology_df.head(3)
#          go_id     go_id.1                              Term Ontology
# 1  GO:0000001  GO:0000001         mitochondrion inheritance       BP
# 2  GO:0000002  GO:0000002  mitochondrial genome maintenance       BP
# 3  GO:0000003  GO:0000003                      reproduction       BP
```

### Example 5: Print the R commands run to stderr with `outstream=True`

```Python
from widediaper import R

# with outstream=True the commands are displayed on stderr
# This makes it easy to see where a script hangs without littering your code with print statements
r = R(outstream=True)
r("p.adjust(c(0.1, 0.1, 0.2, 0.5))")
# p.adjust(c(0.1, 0.1, 0.2, 0.5))
```


### Example 6: Store the R session as a script
```Python
import numpy as np, pandas as pd
from widediaper import R

r = R(session_folder="diaper_contents/")
r("a = 1 + 1")
r("b = a + 1")

!ls diaper_contents/
# widediaper.R

# each session is stored in an R script called <session_folder>/widediaper.R
# to allow the R environment to be easily recreated in an R repl for easier debugging
# it includes print statements to see where exactly the script hangs (if it does)

!head diaper_contents/widediaper.R
# print("a = 1 + 1")
# a = 1 + 1
# print("b = a + 1")
# b = a + 1
```

### Example 7: Ceci n'est pas une pipe

```Python
import numpy as np, pandas as pd
from widediaper import R

r = R(session_folder="diaper_contents/")

df = pd.DataFrame(np.random.rand(3,2))
print(df)
#           0         1
# 0  0.666644  0.341967
# 1  0.843397  0.213561
# 2  0.675183  0.999289

r.send(df, "df_name")
r("print(df_name)")
# u'X0        X1\r\n0 0.6666443 0.3419666\r\n1 0.8433969 0.2135607\r\n2 0.6751825 0.9992890'

same_df_but_been_in_r = r.get("df_name")
print(same_df_but_been_in_r)
#          X0        X1
# 0  0.666644  0.341967
# 1  0.843397  0.213561
# 2  0.675183  0.999289

# dataframes are sent between R and python by persisting them to disk
# this makes the "piping" ultrastable and sessions easier to debug afterwards
!ls diaper_contents/
# from_r_df_name.csv  to_r_df_name.csv  widediaper.R

_ = r.get("mtcars")
!ls diaper_contents/
# from_r_df_name.csv  to_r_df_name.csv  widediaper.R  from_r_mtcars.csv

# afterwards, the widediaper.R script uses the dataframes persisted to perfectly recreate the session
!head diaper_contents/widediaper.R
# print("name = read.delim('/local/home/endrebak/Code/widediaper/diaper_contents/to_r_name.csv', row.names=1)")
# name = read.delim('/local/home/endrebak/Code/widediaper/diaper_contents/to_r_name.csv', row.names=1)
# ...
```

### Example 8: Strings that would make R hang indefinitely are illegal

```Python
r = R()

r('a <- 10 / (3 + 2')
# IllegalRCommandException: Unmatched paranthesis or quote in a <- 10 / (3 + 2. Unable to continue.
```

### Example 9: R errors are reported to stderr

```Python
r = R()

r("x")
Error returned by R call:
 Error: object 'x' not found
```

### Example 10: R errors stops the Python process (by default)

```Python
r = R()

r("stop('Exiting Python!')")
Error returned by R call:
 Error: Exiting Python!

r = R(exit_on_error=False)

r("stop('Python will continue running!')")
 Error: Python will continue running!!
```

## NAQ (NEO!)

> Why would you want to call R from Python?

Python is a more powerful language and using it in preference to R will lead to increases in development speed, programmer sanity, and end user happiness.

To learn why you should care about the power of a programming language, read Paul Graham's wonderful essay ["Beating the Averages"](http://www.paulgraham.com/avg.html). Are you a blub programmer?

> Why don't you just use RPy2?

RPy2 connects to R internals, which means that it is only available for specific R versions and therefore may be hard to install. And when you finally do get it working, you are likely to find that whichever R libraries you were hoping to use are not available for that version of R (R libraries often depend on specific versions of R and have dependencies on other libraries which have yet other dependencies and so on. This means that R libraries can quickly become lost in "dependency hell" due to mutually inconsistent dependencies. This is not something I have encountered once in five years of Python hacking.)


[This stack overflow answerer](http://stackoverflow.com/a/28990370/992687) agrees with me.

> Why don't you just use PypeR

I was for a while, but I could not get it to work in a stable way with large dataframes; pyper would often mysteriously hang.

Some Stack Overflow users have had the [same issue](http://stackoverflow.com/a/13636318/992687) so I suspect this is a common problem.

In addition, there were some quirks with inconsistent handling of strings between python 2 and 3. Lastly, I detest having to depend on untested, monolithic code or projects without an issues page.

> Why the name widediaper?

I wanted to call it pied piper (or PYed pipeR) as an homage to the (company in the) HBO series [Silicon Valley](https://www.youtube.com/watch?v=69V__a49xtw), but this was too similar to PypeR. Therefore I chose one of the scurrilities aimed at the company instead, namely "wide diaper".

The elevated name is a good reminder that this is nothing but a dinky hack.

### Contributing

* Report bugs
* Make documentation requests if something is unclear.
* I'm not interested in growing a much larger set of features, but you are free to make suggestions or fork this project.
* Instead of making a pull request for a new feature, start a discussion on the issue tracker instead.
* Add useful tests written in `py.test`.
* Run the existing tests with `sh tests/run_unit_tests.sh` and `sh tests/run_integration_tests.sh` and report any errors.
* Spread the gospel of wide diapeR
